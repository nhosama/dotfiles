" vifm - Configuration File

"#######################################################
"#  _   _ _   _  ___                                   #
"# | \ | | | | |/ _ \   Naeem Hasan Osama (NHOsama)    #
"# |  \| | |_| | | | |                                 #
"# | |\  |  _  | |_| |  https://www.gitlab.com/nhosama #
"# |_| \_|_| |_|\___/                                  #
"#                                                     #
"#######################################################


" This is the actual command used to start vi.  The default is nvim.
" If you would like to use emacs or emacsclient, you can use them.
" Since emacs is a GUI app and not a terminal app like vim, append the command
" with an ampersand (&).
set vicmd=nvim
"set vicmd=$EDITOR

" This makes vifm perform file operations on its own instead of relying on
" standard utilities like `cp`.  While using `cp` and alike is a more universal
" solution, it's also much slower when processing large amounts of files and
" doesn't support progress measuring.
set syscalls

" Trash Directory
" The default is to move files that are deleted with dd or :d to
" the trash directory.  If you change this you will not be able to move
" files by deleting them and then using p to put the file in the new location.
" I recommend not changing this until you are familiar with vifm.
" This probably shouldn't be an option.
set trash

" This is how many directories to store in the directory history.
set history=100

" Automatically resolve symbolic links on l or Enter.
set nofollowlinks

" With this option turned on you can run partially entered commands with
" unambiguous beginning using :! (e.g. :!Te instead of :!Terminal or :!Te<tab>).
" set fastrun

" Natural sort of (version) numbers within text.
set sortnumbers

" Maximum number of changes that can be undone.
set undolevels=100

" If you installed the vim.txt help file set vimhelp.
" If would rather use a plain text help file set novimhelp.
set novimhelp

" If you would like to run an executable file when you
" press return on the file name set this.
set norunexec

" Selected color scheme
" The following line will cause issues if using vifm.vim with regular vim.
" Either use neovim or comment out the following line.
colorscheme luke

" Format for displaying time in file list. For example:
" TIME_STAMP_FORMAT=%m/%d-%H:%M
" See man date or man strftime for details.
set timefmt=%m/%d\ %H:%M

" Show list of matches on tab completion in command-line mode
set wildmenu

" Display completions in a form of popup with descriptions of the matches
set wildstyle=popup

" Display suggestions in normal, visual and view modes for keys, marks and
" registers (at most 5 files).  In other view, when available.
set suggestoptions=normal,visual,view,otherpane,keys,marks,registers

" Ignore case in search patterns unless it contains at least one uppercase
" letter
set ignorecase
set smartcase

" Don't highlight search results automatically
set nohlsearch

" Use increment searching (search while typing)
set incsearch

" Try to leave some space from cursor to upper/lower border in lists
set scrolloff=4

" Don't do too many requests to slow file systems
if !has('win')
    set slowfs=curlftpfs
endif

" Set custom status line look
set statusline="  Hint: %z%= %A %10u:%-7g %15s %20d  "


" ------------------------------------------------------------------------------

" :mark mark /full/directory/path [filename]

mark h ~/

" ------------------------------------------------------------------------------

" :com[mand][!] command_name action
" The following macros can be used in a command
" %a is replaced with the user arguments.
" %c the current file under the cursor.
" %C the current file under the cursor in the other directory.
" %f the current selected file, or files.
" %F the current selected file, or files in the other directory.
" %b same as %f %F.
" %d the current directory name.
" %D the other window directory name.
" %m run the command in a menu window

command! df df -h %m 2> /dev/null
command! diff vim -d %f %F
command! zip zip -r %f.zip %f
command! run !! ./%f
command! make !!make %a
command! mkcd :mkdir %a | cd %a
command! vgrep vim "+grep %a"
command! reload :write | restart

" ------------------------------------------------------------------------------

" Displaying pictures in terminal
"
" fileviewer *.jpg,*.png shellpic %c

" Open all other files with default system programs (you can also remove all
" :file[x]type commands above to ensure they don't interfere with system-wide
" settings).  By default all unknown files are opened with 'vi[x]cmd'
" uncommenting one of lines below will result in ignoring 'vi[x]cmd' option
" for unknown file types.
" For *nix:
" filetype * xdg-open
" For OS X:
" filetype * open
" For Windows:
" filetype * start, explorer

" ------------------------------------------------------------------------------

" What should be saved automatically between vifm runs
" Like in previous versions of vifm
" set vifminfo=options,filetypes,commands,bookmarks,dhistory,state,cs
" Like in vi
set vifminfo=dhistory,savedirs,chistory,state,tui,shistory,
    \phistory,fhistory,dirstack,registers,bookmarks,bmarks

" ------------------------------------------------------------------------------

" Examples of configuring both panels

" Customize view columns a bit (enable ellipsis for truncated file names)
"
set viewcolumns=-{name}..,6{}.

" ------------------------------------------------------------------------------

" Mappings

" Open the file in EDITOR
map E :!$EDITOR %f<CR>

" Restart ViFM
map R :restart<CR>

" List file opener(s)
nnoremap o :file &<cr>

" Open all images in current directory in sxiv thumbnail mode
nnoremap sx :!sxiv -t %d & <cr>

" Open selected images in gimp
nnoremap gp :!gimp %f & <cr>

" Display sorting dialog
nnoremap S :sort<cr>

" Toggle visibility of preview window
nnoremap w :view<cr>
vnoremap w :view<cr>gv

" Open file in the background using its default program
nnoremap gb :file &<cr>l

" Yank current directory path into the clipboard
nnoremap yd :!echo %d | xclip %i<cr>

" Yank current file path into the clipboard
nnoremap yf :!echo %c:p | xclip %i<cr>

" Mappings for faster renaming
nnoremap I cw<c-a>
nnoremap cc cw<c-u>
nnoremap A cw

" Mapping for lesser keypresses
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
map <C-o> <C-w>o
map <C-s> <C-w>s
map <C-v> <C-w>v

" Start shell in current directory
nnoremap ,s :shell<cr>

" Open console in current directory
nnoremap ,t :!st &<cr>

" Open editor to edit vifmrc and apply settings after returning to vifm
nnoremap ,c :write | edit $MYVIFMRC | restart<cr>

" Toggle wrap setting on ,w key
nnoremap ,w :set wrap!<cr>

" Replace all spaces with underscores in all files's names
nnoremap rr :%s/ /_/g<CR>

" Toggle file selection and go to next file
nmap <space> tj

" Quit ViFM
nmap q ZQ

" Create a directory
map mkd :mkdir<space>

" Set the picture as background
map bg :!setbg %f &<CR>

" Extract the file
map X :!ext %f &<CR>

" Copy directory name
nnoremap yd :!echo %d | xclip %i<cr>

" Copy file name
nnoremap yf :!echo %c:p | xclip %i<cr>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Load shortcuts' file
source ${XDG_CONFIG_HOME:-$HOME/.config}/vifm/vifmshortcuts

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" File Opening
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" The file type is for the default programs to be used with
" a file extension.
" :filetype pattern1,pattern2 defaultprogram,program2
" :fileviewer pattern1,pattern2 consoleviewer
" The other programs for the file type can be accessed with the :file command
" The command macros %f, %F, %d, %F may be used in the commands.
" The %a macro is ignored.  To use a % you must put %%.

" For automated FUSE mounts, you must register an extension with :file[x]type
" in one of following formats:
"
" :filetype extensions FUSE_MOUNT|some_mount_command using %SOURCE_FILE and %DESTINATION_DIR variables
" %SOURCE_FILE and %DESTINATION_DIR are filled in by vifm at runtime.
" A sample line might look like this:
" :filetype *.zip,*.jar,*.war,*.ear FUSE_MOUNT|fuse-zip %SOURCE_FILE %DESTINATION_DIR
"
" :filetype extensions FUSE_MOUNT2|some_mount_command using %PARAM and %DESTINATION_DIR variables
" %PARAM and %DESTINATION_DIR are filled in by vifm at runtime.
" A sample line might look like this:
" :filetype *.ssh FUSE_MOUNT2|sshfs %PARAM %DESTINATION_DIR
" %PARAM value is filled from the first line of file (whole line).
" Example first line for SshMount filetype: root@127.0.0.1:/
"
" You can also add %CLEAR if you want to clear screen before running FUSE
" program.

" Pdf, PostScript, Djvu, EPub, Comic Book Archive (cb*)
fileviewer *.pdf,*.ps,*.eps,*.ps.gz,*.djvu,*.epub,*.cbz,*.cbr,*.cb7
        \ vifmimg pdfpreview %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear
        " \ pdftotext -nopgbrk %c -
filextype *.pdf,*.ps,*.eps,*.ps.gz,*.djvu,*.epub,*.cbz,*.cbr,*.cb7 zathura %f 2>/dev/null &,

" Audio
filetype *.wav,*.mp3,*.flac,*.m4a,*.wma,*.ape,*.ac3,*.og[agx],*.spx,*.opus
        \ {View using cmus}
        \ alacritty -e cmus %f &,
       \ {Play using ffplay}
       \ ffplay -nodisp -autoexit %c,
       \ {Play using MPlayer}
       \ mplayer %f,
fileviewer *.mp3 mp3info
fileviewer *.flac soxi

" Video
fileviewer *.avi,*.mp4,*.wmv,*.dat,*.3gp,*.ogv,*.mkv,*.mpg,*.mpeg,
	\*.vob,*.fl[icv],*.m2v,*.mov,*.webm,*.ts,*.mts,*.m4v,*.qt,
	\*.divx,*.as[fx]
	\ {Show preview}
       " \ vifmimg videopreview %px %py %pw %ph %c
       " \ %pc
       " \ vifmimg clear
	\ {Show file info}
	\ file
filextype *.avi,*.mp4,*.wmv,*.dat,*.3gp,*.ogv,*.mkv,*.mpg,*.mpeg,
	\*.vob,*.fl[icv],*.m2v,*.mov,*.webm,*.ts,*.mts,*.m4v,
	\*.r[am],*.qt,*.divx,*.as[fx]
	\ {View using mpv}
	\ mpv %f 2>/dev/null &,

" Web
filextype *.html,*.htm
        \ {Open with emacs}
        \ emacsclient -c %c &,
        \ {Open with vim}
        \ vim %c &,
        \ {Open with dwb}
        \ dwb %f %i &,
        \ {Open with firefox}
        \ firefox %f &,
        \ {Open with uzbl}
        \ uzbl-browser %f %i &,
filetype *.html,*.htm
	\ elinks,
	\ lynx,
	\ links
fileviewer *.html
	\ w3m -dump %c

" Object
filetype *.o
	\ nm %f | less

" Man page
filetype *.[1-8]
	\ man ./%c
fileviewer *.[1-8]
	\ man ./%c | col -b

" Images
filextype *.bmp,*.jpg,*.jpeg,*.png,*.ico,*.gif,*.xpm
       " \ {View in sxiv}
       " \ sxiv -ia %f &,
	\ {View in sxiv - Luke's Method}
	\ rotdir %f 2>/dev/null | sxiv -ia 2>/dev/null &
        \ {View in imv}
        \ imv -b 1D2330 -d %d &,
        \ {View in feh}
        \ feh %d &,
        \ {View in cacaview}
        \ cacaview %c &,
fileviewer *.bmp,*.jpg,*.jpeg,*.png,*.xpm,*.ico,*.gif
        \ vifmimg draw %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

" OpenRaster
filextype *.ora
        \ {Edit in MyPaint}
        \ mypaint %f,

" Mindmap
filextype *.vym
        \ {Open with VYM}
        \ vym %f &,

" MD5
filetype *.md5
       \ {Check MD5 hash sum}
       \ md5sum -c %f %S,

" SHA1
filetype *.sha1
       \ {Check SHA1 hash sum}
       \ sha1sum -c %f %S,

" SHA256
filetype *.sha256
       \ {Check SHA256 hash sum}
       \ sha256sum -c %f %S,

" SHA512
filetype *.sha512
       \ {Check SHA512 hash sum}
       \ sha512sum -c %f %S,

" GPG signature
filetype *.asc
       \ {Check signature}
       \ !!gpg --verify %c,

" Torrent
filetype *.torrent
	\ ktorrent %f &
fileviewer *.torrent
	\ dumptorrent -v %c

" FuseZipMount
filetype *.zip,*.jar,*.war,*.ear,*.oxt,*.apkg
       \ {Mount with fuse-zip}
       \ FUSE_MOUNT|fuse-zip %SOURCE_FILE %DESTINATION_DIR,
       \ {View contents}
       \ zip -sf %c | less,
       \ {Extract here}
       \ tar -xf %c,
fileviewer *.zip,*.jar,*.war,*.ear,*.oxt zip -sf %c

" ArchiveMount
"filetype *.tar,*.tar.bz2,*.tbz2,*.tgz,*.tar.gz,*.tar.xz,*.txz
"       \ {Mount with archivemount}
"       \ FUSE_MOUNT|archivemount %SOURCE_FILE %DESTINATION_DIR,
"fileviewer *.tgz,*.tar.gz tar -tzf %c
"fileviewer *.tar.bz2,*.tbz2 tar -tjf %c
"fileviewer *.tar.txz,*.txz xz --list %c
"fileviewer *.tar tar -tf %c

" Archive
fileview *.tar.gz atool -l --format=tar %f 2>/dev/null | awk '{$1=$2=$3=$4=$5=""; print $0}'
fileview *.zip,*.cbz    atool -l --format=zip %f 2>/dev/null | tail +4 | awk '{$1=$2=$3=""; print $0}'
fileview *.rar,*.cbr    atool -l --format=rar %f 2>/dev/null | tail +9 | awk '{$1=$2=$3=$4=""; print $0}'
fileview *.7z,*.cb7     atool -l --format=7z %f 2>/dev/null | tail +20 | awk '{$1=$2=$3=$4=$5=""; print $0}'
filetype *.zip,*.7z,*.rar,*.tar.gz,*.tar.xz ext %f


" Rar2FsMount and rar archives
filetype *.rar
       \ {Mount with rar2fs}
       \ FUSE_MOUNT|rar2fs %SOURCE_FILE %DESTINATION_DIR,
fileviewer *.rar unrar v %c

" IsoMount
filetype *.iso
       \ {Mount with fuseiso}
       \ FUSE_MOUNT|fuseiso %SOURCE_FILE %DESTINATION_DIR,

" SshMount
filetype *.ssh
       \ {Mount with sshfs}
       \ FUSE_MOUNT2|sshfs %PARAM %DESTINATION_DIR %FOREGROUND,

" FtpMount
filetype *.ftp
       \ {Mount with curlftpfs}
       \ FUSE_MOUNT2|curlftpfs -o ftp_port=-,,disable_eprt %PARAM %DESTINATION_DIR %FOREGROUND,

" Fuse7z and 7z archives
filetype *.7z
       \ {Mount with fuse-7z}
       \ FUSE_MOUNT|fuse-7z %SOURCE_FILE %DESTINATION_DIR,
fileviewer *.7z 7z l %c

" Office files
filextype *.odt,*.doc,*.docx,*.xls,*.xlsx,*.odp,*.pptx libreoffice %f &
fileviewer *.doc catdoc %c
fileviewer *.docx docx2txt.pl %f -

" TuDu files
filetype *.tudu tudu -f %c

" Qt projects
filextype *.pro qtcreator %f &

" Graphics Files (GIMP, Inkscape)
filextype *.svg inkscape %f 2>/dev/null &
filextype *.xcf gimp %f 2>/dev/null &

" Directories
fileview */,.*/,../ tree %c -L 1 --dirsfirst


" ------------------------------------------------------------------------------

" Various customization examples

" Use ag (the silver searcher) instead of grep
"
" set grepprg='ag --line-numbers %i %a %s'

" Add additional place to look for executables
"
" let $PATH = $HOME.'/bin/fuse:'.$PATH

" Block particular shortcut
"
" nnoremap <left> <nop>

" Export IPC name of current instance as environment variable and use it to
" communicate with the instance later.
"
" It can be used in some shell script that gets run from inside vifm, for
" example, like this:
"     vifm --server-name "$VIFM_SERVER_NAME" --remote +"cd '$PWD'"
"
" let $VIFM_SERVER_NAME = v:servername

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
